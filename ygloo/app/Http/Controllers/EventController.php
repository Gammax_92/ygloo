<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\model\Event;

class EventController extends Controller
{
    public function index()
    {
        $event = Event::all();

        return response()->json($event);
    }
}
